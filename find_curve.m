function [points, visited] = find_curve(bw, angle, start_point_index, in_visited)
next_x = [-1 -1 -1; 0 0 0; 1 1 1];
next_y = [-1 0 1; -1 0 1; -1 0 1];
visited = in_visited;
[range_x, range_y] = size(bw);
angle = angle + 90;
vx = cos(angle / 180.0 * pi);
vy = sin(angle / 180.0 * pi);
% 90
constraint = 0;
[px, py] = ind2sub(size(bw), start_point_index);
points = start_point_index;
while true
    val = 0.00001;
    val_index = 0;
    for i = 1:9
        if i ~= 5
            dx = next_x(i);
            dy = next_y(i);
            x = px + dx;
            y = py + dy;
            A = [vx vy; dx dy];
            %fprintf('%d %d %d %d\n', x, y, range_x, range_y);
            if ((x > 0) && (y > 0) && (x <= range_x) && (y <= range_y))
                index = sub2ind(size(bw), x, y);

                %fprintf('%d %d %f %f %d\n', x, y, bw(index), 1 - pdist(A, 'cosin'), visited(index));
                if (1 - pdist(A, 'cosin') > constraint) && (visited(index) == 0)
                    if bw(index) > val
                        val = bw(index);
                        val_index = index;
                    end
                end
            end
        end
    end
    
    if (val_index == 0) || (length(points) > 40)
        break;
    else
        visited(val_index) = 1;
        points = [points val_index];
        [x, y] = ind2sub(size(bw), val_index);
        vx = x - px;
        vy = y - py;
        %fprintf('%f %d %d\n', val, vx, vy);
        px = x;
        py = y;
    end
    constraint = -0.1;
end