function curve_detec(bw,sed_x,sed_y,plot)
    matrix = neighbor(bw,sed_x,sed_y)
    [m,ind] = max(matrix(:));
    
    [next_x,next_y] = ind2sub(size(matrix),ind)
    next_x = sed_x + next_x -2;
    next_y = sed_y + next_y -2;
    
    if bw(next_x,next_y) < 0.2
        return;
    end
    
    [dim_x,dim_y] = size(plot);
    contain = 0;
    for i =1:dim_x
        if plot(i,1) == next_x && plot(i,2) == next_y
            contain = 1;
            break;
        end
    end
    
    if contain == 0
        plot = [plot;next_x next_y];
        curve_detec(bw,next_x,next_y,plot);
    end
    