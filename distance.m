function [dis] = distance(bw,points,label)
    subs = [];
    for i=1:length(points)
        [p_x,p_y] = ind2sub(size(bw),points(i));
        subs = [subs ;p_x p_y];
    end
    
    dis = zeros(length(points));
    for i=1:length(points)
        for j=1:length(points)
            dis(i,j) = (subs(i,1)-subs(j,1)).^2 + (subs(i,2)-subs(j,2)).^2;
            if dis(i,j)==0
                dis(i,j) = 1000000;
            end
            
            if label(points(i))==label(points(j))
                dis(i,j) = 1000000;
            end
        end
    end