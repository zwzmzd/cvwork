function [Gmag, Gdir] = curve(I)
Im = rgb2gray(I);
[Gmag, Gdir] = imgradient(Im,'prewitt');