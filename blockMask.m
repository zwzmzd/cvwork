function OUT = blockMask(Image)

bImage = im2bw(Image, 240.0 / 255);
OUT = 1 - bImage;

