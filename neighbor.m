function matrix=neighbor(bw,index_x,index_y)
    matrix = zeros(3,3);
    outsize_x = 0;
    outsize_y = 0;
    [m,n] = size(bw);
    if index_x < 2
        outsize_x = -1;
    elseif index_x > m-1
        outsize_x = 1;
    end
    
    if index_y < 2
        outsize_y = -1;
    elseif index_y > n-1
        outsize_y = 1;
    end
    
    if outsize_x ~= -1 && outsize_y ~= -1
        matrix(1,1) = bw(index_x-1,index_y-1);
    end
    
    if  outsize_x ~= -1
        matrix(1,2) = bw(index_x-1,index_y);
    end
    
    if outsize_x ~= -1 && outsize_y ~= 1
        matrix(1,3) = bw(index_x-1,index_y+1);
    end
    
    if  outsize_y ~= -1
        matrix(2,1) = bw(index_x,index_y-1);
    end
    
    if outsize_y ~= 1
        matrix(2,3) = bw(index_x,index_y+1);
    end
    
    if outsize_x ~= 1 && outsize_y ~= -1
        matrix(3,1) = bw(index_x+1,index_y-1);
    end
    
    if outsize_x ~= 1
        matrix(3,2) = bw(index_x+1,index_y);
    end
    
    if outsize_x ~= 1 && outsize_y ~= 1
        matrix(3,3) = bw(index_x+1,index_y+1);
    end