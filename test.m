addpath(genpath(pwd));

I = imread('data\apple.png');
Im = rgb2gray(I);

% 区域划分,模版腐蚀
figure(1);
mask = blockMask(Im);
bbw = edge(Im, 'canny', 0.2);
subplot(2,2,1); imshow(bbw); title('original');

subplot(2,2,3);imshow(mask); title('mask');

se = strel('diamond', 7);
mask_b = imerode(mask, se);
subplot(2,2,4); imshow(mask_b); title('diamond');

label = bwlabel(mask_b);
subplot(2,2,2); vislabels(label);
title('划分后的区域');

%
%
%
figure(2);
bw = bbw;
subplot(1,2,1); imshow(bw); title('original');

% 掩模的边界位置
mask_edge = edge(mask_b, 'canny');
set_a = find(mask_edge > 0);
% 梯度图像上值显著位置, 参数挑出来的
set_b = find(bw > 0.0975);
% 既在掩模边界上，而且梯度值比较大的位置
set_c = intersect(set_a, set_b);
[px, py] = ind2sub(size(bw), set_c);

%得到边界的梯度方向，近似是边缘曲线的法向
[nouse, dir] = imgradient(mask_b,'prewitt');

% 将原图点乘掩模，去除原本的边界
bw2 = bw .* mask_b;
subplot(1,2,2); imshow(bw2); title('masked image');

% 绘制出刚刚标定的点
hold on;            %# Add subsequent plots to the image
plot(py, px, '.', 'Markersize',5,'color',[1 0 0])
hold off;           %# Any subsequent plotting will overwrite the image!

%
%
%
visited = im2bw(zeros(size(bw)));
visited(set_a) = 1;
point_set = set_c;

count = 0;
curve_image = im2bw(zeros(size(bw)));
curve_image(set_a) = 1;
start_point_set = [];
for i = 1:length(point_set);
    index = point_set(i);
    [points, visited] = find_curve(bw, dir(index), index, visited);
    if length(points) > 20
        count = count + 1;
        curve_image(points) = 1;
        start_point_set = [start_point_set index];
        select = [1:2:30];
        points_select = points(select);
        [bezier_x,bezier_y] = ind2sub(size(bw),points_select);
        %bezier_y = besselj(0,bezier_x);
        [b_x,b_y] = bezier(bezier_x,bezier_y);
        %plot(b_x,b_y);
        %grid on;
    end
end
figure(3);
imshow(curve_image);

dist = distance(bw,start_point_set,label);
[value , paire_ind] = min(dist);
color_kind = [1 0 0;0 1 0;0 0 1;0.5 0 0;0 0.5 0;0 0 0.5;0.5 0.5 0.5;]

% colors = [1 0 0;
%           1 0 0;
%           0 1 0;
%           0 0 1;
%           0 1 0;
%           0 0 1;
%           0.5 0 0;
%           0.5 0 0;
%           0 0.5 0;
%           0 0 0.5;
%           0 0.5 0;
%           0.5 0.5 0.5;
%           0 0 0.5;
%           0.5 0.5 0.5];
pair = zeros(1,14);
begin = 1;
for i=1:14
    if pair(i)== 0
        pair(i) = begin;
        pair(paire_ind(i)) = begin;
        begin = begin + 1;
    end
end
    
[px, py] = ind2sub(size(bw), start_point_set);
hold on;            %# Add subsequent plots to the image
for i=1:length(px)
    plot(py(i), px(i), 'o', 'Markersize',5,'LineWidth', 2, 'color',color_kind(pair(i),:))
end
hold off;           %# Any subsequent plotting will overwrite the image!

    